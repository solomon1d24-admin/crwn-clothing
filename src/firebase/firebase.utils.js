import firebase from 'firebase/compat/app';
import 'firebase/compat/firestore';
import 'firebase/compat/auth';




const config = {
    apiKey: "AIzaSyB8ioJuwd4r-DPtasUsIU4f8NsJ9ytE21k",
    authDomain: "crwn-db-ad580.firebaseapp.com",
    projectId: "crwn-db-ad580",
    storageBucket: "crwn-db-ad580.appspot.com",
    messagingSenderId: "699495761565",
    appId: "1:699495761565:web:bc76a6cae1dfd6366d3674",
    measurementId: "G-XM009T9BYY"
  };

  export const createUserProfileDocument = async(userAuth, additionalData) =>{
    if(!userAuth){
      return;
    }
      const userRef = firestore.doc(`users/${userAuth.uid}`);

      const snapShot= await userRef.get();

      if(!snapShot.exists){
        const { displayName, email } = userAuth;
        const createdAt = new Date();
        try{
          await userRef.set({
            displayName,
            email,
            createdAt,
            ...additionalData
          }) 
        } catch(error){
          console.log('error creating user', error.message)
        }
      }
      return userRef;

  }


firebase.initializeApp(config);

export const auth = firebase.auth();
export const firestore = firebase.firestore();

const provider = new firebase.auth.GoogleAuthProvider();
provider.setCustomParameters({ promot: 'select_account'});
export const signInWithGoogle = () => auth.signInWithPopup(provider);
export default firebase;